---
title: "Lenses"
date: 2020-04-14T10:48:00-04:00
draft: false
---

I've been obsessed with the idea of first principles and mental models as of late. Today at work, I was able to give a name to an approach I sometimes find myself taking when reviewing code: I use different lenses. It made me very happy to realize that this idea is not new and is related to first principles and mental models. Let me explain.

### Reviewing Code
When reviewing code for someone, it's very easy to find yourself looking for certain patterns that you've seen before. While this is all good and well, it has the potential to blind you to other problems. Conversely, sometimes you find yourself reviewing code with a different point of view. Sometimes, you're very interested in the performance of the code rather the readability of the code. Sometimes, you have so much energy to review code you're a bug busting machine able to point out even the most minute of logic errors.

Ideally, you're reviewing code like this all the time, however reality is generally far from ideal. The thing I was pleased to realize is that, just like when we review code, we use different lenses in our daily lives and we do ourselves a huge favor if we *purposefully* use different lenses.

### Why it Matters
I suspect that, like most things, there is a balance to strike when using lenses. For instance, tunnel vision can be useful and detrimental. Useful when you need razor sharp focus but less so when you miss the big picture.

Consider any project you happen to be working on. There are times when you have to sweat the small stuff but there are other times when you really need to take a step back and evaluate what the next most critical action is. Make sense?

Now, this is all fine and well, we've briefly chatted the distinction between detailed, focused work and big picture thinking. However, there are many shades of these differing viewpoints. For instance, sometimes the work that must be done presents the opportunity to evaluate the efficiency of how you'll actually do the work. It's true that there are times when you just need to be "heads down" and bulldoze your way through the job, however, other times it's worth understanding that you have an option for a better way, a more efficient way.

Why does it matter? Simply because you can create a better outcome for whatever it is you're doing. You can present a more well considered solution. You may even find that your original solution wasn't all that good. All this holds true not only for the work you create, but for the work and output of others that you evaluate. Using a different lens during evaluation gives you new perspectives that can often times be immensely valuable to you, the project, the solution, and those around you.

### Examples
#### Reviewing code
Since this blog trends towards programming, I'll start with the easiest application for me first: code reviews. For the uninitated, code reviews happen when one programmer reviews the work of another programmer before merging the new work with the existing work. It's a system of checks and balances; an attempt to make sure we only move forward with good work. Now, when a programmer is reviewing code, they may default to a review of logic (hopefully). But logic is not the only lens we can, or should, use when we're looking for good work. Code, is used by computers and read by humans. So what about the readability of code? In some cases, the problem to solve requires sheer speed and performance. How does the need for speed balance with the readability? Should it? What if we look to see if the code actually solves the intended problem? Or solves it well? Or solves the original problem but creates a brand new one?

These are all real possibilities. Possibilities that, unless you're thinking with different lenses, you just might miss.

#### Relationships and Communication
Managing our relationships and communication with others benefits greatly from this idea of lenses. How many times have we argued or misunderstood someone else, only to find out later how bad we errored in our misunderstanding? Plenty of times I'm sure. Each time we do this, it is likely attributable to the fact that we are all (shocker) different! We have matured in different environments, had different experiences, been exposed to different information. All of these differences shape how our brains are built so, naturally, the way we perceive and communicate is different. This is great for diversity and problem solving, however, sometimes we forget this in the moment.

If we can train ourselves to remember these differences in the moment that we're communicating with someone, perhaps we can pause long enough to more fully consider what the other person is saying. Perhaps we can consider that maybe they're just having a bad day and not communicating well? Perhaps they didn't get enough sleep? Perhaps they've experienced this problem before and know something we don't? Perhaps they have biases trained into them that they're unaware of?

The ability to change lenses during communication with others can go a long way to better relationships and better problem solving.

### Changing Lenses
So how do we purposefully change lenses? This is still an ongoing experiment for me, however, I think there are a few ways we can make this easier on ourselves.

First, review your past day or week and figure out what lenses you might have applied. Were there any opportunities that, in hindsight, you could have approached differently?

Secondly, once we identify lenses we want to integrate, we should plan on ways to actually try and use them in the moment. For instance, I recently noticed a communication difference between myself and others. After some effort thinking through what those differences are, I decided to feel ok with (politely) interrupting someone when I realize they are going down a pre-defined path of communication that doesn't work for my understanding. It's been very interesting to see how this has actually helped create clarity in conversations.

Third, study other domains and mental models. Read, watch videos, listen to podcasts, or speak with people that are working outside of your normal domain. What problems are they working through? What solutions are they coming up with? What are the first principles that person or domain starts with? There is a lot to gleam from these sources; use them.

Fourth, prompt yourself with some simple questions. What is the purpose of this thing? What are some other ways I can think about this that are different from my initial gut reaction?

Finally, study history. Lenses and perspectives are easy to come by when we're dealing with history. We have the benefit of hindsight so as long as we're thoughtful about it, we can find a new point of view or what lens might have been helpful in that particular scenario.
