---
title: "Multi Paradigm Programming"
date: 2019-03-23T10:03:41-04:00
draft: false
---
My current view of software engineering sees a lot of paradigm camps. Functional vs OO being the primary contenders right now. It's easy to forget that there are, in fact, other paradigms. It's always confused me why there should be multiple paradigms and when should I choose one over the other. The consensus usually leads us to believe "there can be only one", and when we choose the one paradigm, then all our code should be guided by that paradigm.

That's not true. I think we can and should be using a multi-paradigm programming style.

One of the first repeated patterns I've witnessed that led me to this conclusion was MongoDB, or more generically, document databases. As is standard in the industry, something seemingly new was introduced and we dutifully responded by using it everywhere. The problem is that document databases aren't always the correct choice. Sometimes, data is simply relational in nature and document databases have a tough time coping once the data has reached a certain size/complexity. The takeaway? Sometimes, the data is relational and so it should be stored in a relational way.

That's pretty key to this idea: any problem that we work on has a "best" or most efficient way that it wants to be solved. If this is true, then the next takeaway is: a single problem can be made up of multiple subproblems and each subproblem might have it's own "best" solution.

An easy example to look at is a modern day web app. Think about the flow of the data: the request comes into the server, runs through middleware, is delivered to your business logic, queries the database, and formulates a response. Pretty typical.

If we break this down to it's parts, we might think of the flow of data like this:

1. Request received and run through middleware <-- Procedure
2. Request delivered to business logic/database <-- Object
3. Logic within the objects is run <-- Functional

Each of these parts of the application are a different problem to be solved. Delivering the data, relating the data, working on the data, deriving new data, etc. Why should we mentally limit ourselves to a single paradigm when using our favorite language? Most modern day, high level languages support multiple paradigms; it's almost like we were meant to be doing this from the start.

In fact, I think we should be deliberately using multiple paradigms; it's just taken me this long to realize it. Reviewing old code, I've found that I'm already doing this naturally in some cases, but now I'm viewing this as a deliberate tool.

Try looking at your coding problems with a multi-paradigm programming lens.
