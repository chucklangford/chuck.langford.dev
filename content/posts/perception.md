---
title: "Perception"
date: 2021-05-09T11:24:00-04:00
draft: false
---

I've never really been comfortable with the Object Oriented (OO) coding paradigm; logic was never an issue but the structure and detail of OO has always been a confusing mess to me. This essay is an attempt to map my way of thinking to the OO way of thinking. The outcome of this exercise, however, was something broader than just thinking about programming. Now I'm thinking about perception.

## How I Think
The great personal benefit of this exercise is that it really forced me to consider how I think. Turns out, I'm obsessed with a very narrow part of reality: how (algorithms) and why (need) stuff works. I use the term "stuff" very specifically here because my thought process doesn't include classification or names of items, persons, or places. My thought process is most occupied with why and how. The things, the people, the objects involved in a process take a back seat to the why.

This is a great way to think when you're diagnosing an issue. It's a great way of thinking when you consider systems in general. It's a great way to analyze and discover the truth about a subject. It can seem like all problems are laid bare under the scrutiny of "how?" and "why?". However, this approach leaves me open to blind spots.

How and why doesn't provide a complete picture of reality. Nouns exist. They have names, and classifications and different ways of interacting with each other. These concepts don't necessarily give us the truth of how and why, but they are still true. Furthermore, these things tend to be the primary way we all perceive the world in our daily lives: *I* turn off the *alarm*, get out of *bed*, and make *coffee*. We interact with things via very specific means. We're sometimes able to substitute one item for another. We intuitively know that some items simply won't work with other items. We detect patterns that help us navigate our interactions with objects and other people. I think of these ideas as the who/what truth.

Essentially, my default way of thinking is a limited, albeit very useful, view of the world.

## How to Map My Thinking
It turns out, my way of thinking doesn't need to be "mapped" to another way of thinking, it needs to make room for a fuller picture of reality. This is going to take some work on my part but it will be worth the effort. Instead of being singularly driven by how and why, I'll add who, what, and where to the thought process.

In the most practical of terms, I'm going to make a point of more frequently asking who, what, when, and where.

## Perception
The broader principle here is perception. How you approach a problem, the world, a situation, or a person all hinges on your perceptions. Your perceptions are based only on what you know (or think you know). "Why" is a great question to get to the "root" of a subject. It's just not the complete picture. It doesn't convey all the actors, or how they relate and interact with one another. It doesn't allow you to see patterns and interactions that can inform you and allow you to move quickly.

Similarly, being concerned with only the actors is just as limiting. Who/What thinkers will find repeating patterns as they encounter their world. This allows them to quickly navigate what they encounter as they can transfer patterns of understanding from one group to the next. However, the world is full of uniqueness. Similar groups can have their own version of "why". This lack of understanding can lead to a wrong move or implementation.

## The Value of a More Complete Perception
As a programmer it's obvious to say that the more complete your understanding the better your code will be. Generalizing this idea, however, gives us something very valuable to consider in all walks of life. Complete perception...:

* allows you to autonomously respond in the best way possible
* coordinates people
* allows you to connect ideas and thoughts which accelerates learning and change

*The Challenge:* Take a few minutes to inventory how you typically learn about a person, a problem, current events, the world at large. Ask yourself if you take action based on learned patterns or if you drive yourself to understand the why. Do you take a balanced approach? Or do you typically skew one way or another? Does your thought process always work? Or have you ever missed something that, later, altered your understanding?

