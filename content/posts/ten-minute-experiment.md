---
title: "The Ten Minute Experiment"
date: 2020-03-29T17:25:12-05:00
draft: false
---

I've been working towards teaching myself to deliberately shift focus in order to maintain balance and to just simply get more done. Not just for work-life balance, but a balance between all the things I need to focus on: family, health, career, finances, etc. So, I've setup a spreadsheet to help me keep track of these various facets and so far, it's been very useful to help prompt myself with reminders.

This week, I'm attempting to incorporate the idea of 10 minute intervals in each (most) of these facets. I get the feeling that I'm likely to focus on too much and potentially degrade the benefit of "focus time", however, the goal remains the same: train myself to shift focus to maintain balance rather than than never shifting focus and lose balance.

What follows is a rough diary format...

### Day 1:
I've found that 10 minutes is an interesting amount of time. It's short enough that you don't mind doing chores and getting stuff done but just long enough that you can make progress. I definitely felt that I have scheduled too many subjects to focus on but, on the flip side of the same coin, I made progress on all those subjects. Granted, it's a Sunday so I own my time but still, I certainly hope I own 80 minutes a day during the work week...

### Day 2:
I need a system or habit to help keep me from allowing days like today to take over. Ten hour day, no breaks, and very little time to devote to studies before or after work. Obligations have to be upheld; this is at least one excellent reason why a narrow focus is recommended.

### Day 3:
Had a enough energy this morning to work a large part of my list this morning before work. It seems to me that this method is great for making small progress on a great many fronts. However, I also think that this method may not account for flow or deep work. I might also suggest this method for someone that is simply trying to refresh on a subject they already have some skill in. And none of this means you can't simply work more than the allotted 10 minutes.

### Day 4:
Energy level was not the same as yesterday. Definitely not feeling it today so won't punish myself if I don't get everything done.

### Day 5:
Found that I need to make a concerted effort to integrate some of my 10 minute practice sessions into work. This should be possible, I just need to plan better to integrate the needs of the work project with what I'm currently studying. I also discovered that if, properly broken down, some of my 10 minute practice sessions are fairly easy to master and I need to move on a bit quicker. This just means I should plan more at the start of the week.

### Day 6:
It's a Friday so that naturally means my motivation is dropping off to some extent. Overall, I'm super please with how this experiment has gone and I think I've learned some things I need to refine. First, it's hyper critical that planning be done before hand and be done well. Secondly, it's important to have backup plans in case the planning doesn't match reality; we sometimes blow through learning rather quickly and we sometimes discover our plans/material isn't all that great. Finally, I think I need to try even more so to make my practice line up with what I actually do and/or use every day at work or home.

### Day 8:
I missed a day, but this has been an interesting experiment. Looking back on this week, I've accomplished at least 60 minutes of practice in Rust, Python, SQL, math, writing, guitar, and philosophy. I initally had doubts (and maybe still do) that this was way too many subjects to accomplish in one week. However, an alternative way of looking at this is these are all complementary studies that may need only minor refinement.

If you compare the ideas of practice on an instrument, you'll find that a musician doesn't just practice scales. Instead, the musician practices scales, chords, songs, melodies and other areas that need improvement. Similarly, the practice of a few technology stacks can be a complementary practice.

Furthermore, not only is 10 minutes just long enough to be useful for progression, it's also just short enough when you realize you're working with less than ideal material. Thoughout this experiment I realized that some of the material I had picked to work with wasn't that ideal for me. Ten minutes is just about enough time to figure out that you should move on.

Overall, I plan to keep refining and using using this method.
