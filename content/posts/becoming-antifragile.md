---
title: "Becoming Antifragile"
date: 2020-04-20T08:22:00-04:00
draft: false
---

I'm currently reading Anti-fragile by Nassim Nicholas Taleb. I'm not even close to done but I'm fascinated with the idea so far: antifragility exists when something is made stronger through trial, stress, difficulty, or pain. This is an awesome idea to consider, especially when you're building systems. Is it possible to build a system that is specifically antifragile? Maybe, I don't know; I haven't finished the book yet. ;)

What follows are my thoughts as I'm going through the book. If you're here to find a complete or cohesive thought about antifragility, you're in the wrong place. I expect I'll be creating new posts as I continue through the book.

### Systems and Antifragility
Without going into a detailed discussion of systems, let's review how systems relate to the idea of antifragility.

Systems have stock. A buffer of whatever the system's primary "currency" is: data, objects, energy, strength, time, etc. A system is antifragile when an event happens that may be perceived as negative or volatile but in actuality, the system responds by adding more to it's own stock.

That may sound like a sort of perpetual motion machine which is impossible so in a system we know there is a limit. The pain, or the negative event cannot be sustained or exponential; the sytem likely is using it's stock to counter the event and is rebuilding it's stock but if the event outpaces the systems ability to restock, then the system will fall over. This tells us that volatility cannot be sustained, the system must have a period of rest in order to make itself stronger.

This also means that if we want to increase the strength of a system, we need to expose it to frequent, but small, episodes of volatility. Think of this as exercising the system.

### Muscles and Learning
Two example domains that can help us understand this idea is muscles and learning. Both of these domains exhibit antifragility that we can easily understand.

It's common knowledge that exercise increases our health. Specifically, resistance training increases the strength of muscles. Under load, the muscles strain to keep up. They expend their stock of energy to move the weights. On a microscopic level, the muscles even undergo some damage, but once that load is done and the muscles are allowed to rest, something very extraordinary and useful happens: the muscles get stronger.

The same pattern can be seen when learning something new. When you begin your concerted effort to learn, you may typically find it difficult, almost painful. New concepts can be quite tricky for the brain to understand and manage. However, over time and with proper rest, you find that the same concepts come almost naturally, like you've always known them. The brain has rewired itself to the subject matter you've been studying. It has made recall more efficient. The brain became "smarter".

### Lessons
What do we learn from this idea so far? (Remember, I'm still reading the book):

* Discomfort is a catalyst for growth. Maybe not in all scenarios and maybe it's not the only catalyst, but we should definitely pay attention when we see it.
* Seek to take risk in each domain of life; small enough that the system is not overwhelmed but large enough to cause a response.
* Cadence, and variety of discomfort will change per domain.

### Naive Action Plan
In thinking about antifragility and building an antifragile computer system, I asked my peers for their thoughts on how to go about it. The conversation helped me realize something: when attempting to think about building something to be antifragile, you first need to define what antifragile means for the the thing you want to build. For instance, when building a web application, what does antifragle mean? The application runs fast? The application scales? The application never crashes? The application never errors? The application handles data more efficiently? It's easy to say "yes" to all of these but we're just running a thought experiment here: how can an application be antifragile? How can it "get better" when it experiences volatility?

This leads me to the beginning of an action plan:

* Decide what "stronger" or "better" means for the system you're building.
* Decide what volatility will trigger your system's antifragile response.
* Decide what is the appropriate cadence and ratio of volatility to rest for the system you're building.
* Decide how you can intentionally inject that volatility into your system at a controlled rate.
