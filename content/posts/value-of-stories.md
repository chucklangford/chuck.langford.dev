---
title: "The Value of Stories"
date: 2019-12-15T18:53:12-05:00
draft: false
---

**TLDR:** We all have our own stories, our own desires, and our own reasons and sometimes price is a part of that story. Creatives help us share our stories, fulfill our desires, and validate our reasons.

Every week I attend a group meeting for creative entrepreneurs. The goal of the meeting is to raise the level of creative thought and discussion in the area. These meetings cover a lot of ground and there's always something very interesting to learn; the last meeting was no exception.

The subject of Comedian came up. If you're unaware, Comedian is a sculpture by Marizio Cattelan and it has sparked controversy. The reason? Comedian is an actual banana duct-taped to a wall and it sold for approximately $120,000.

When the entrepreneurial group began to discuss this topic, we almost unanimously agreed that this was ludicrous. However, our group leader had an entirely different opinion. He announced surprise at our reaction because Comedian was an extreme example of what each creative entrepreneur is trying to achieve. The discussion continued without resolution before I had to leave.

I've been thinking about it ever since. This post explains my reasoning behind why I now agree with the group leader and why I think every creative entrepreneur needs to understand Comedian.

### Reality Check
Before diving into the argument, I think it's important to address what might be the most pressing concern of anyone reading this: selling a banana and a piece of duct tape for $120,000 is absurd. There's really no sense in denying it. Logically, and mathematically it makes absolutely no sense.

Let's also address the fact that this was never meant to be for everyone. The large price tag automatically excluded most of us.

It's my belief that the price tag was actually a part of the sculpture's purpose; I'll explain.

### History
I'm not an art buff but a little reading tells me Cattelan is no stranger to challenging ideas. From using a taxidermied horse as a chandelier to a gold cast working toilet to suspending all his previous pieces in one large display, Cattelan's work challenges us.

There's a bit of a theme though. These pieces attempt to challenge us to think about what we value. The chandelier horse might challenge you to consider that no matter how elegant or successful we all attempt to be, we're all heading to the same conclusion. The working, and publicly accessible, gold toilet might challenge you to consider how we degrade that which has actual value. All of Cattelan's work hung in an irreverent way might push us to consider how valuable any of our work really is.

A banana duct-taped to a wall with a $120,000 price tag might ask us to consider how much value we place on the transient. In this case, price is a part of the sculpture. Cool, thought provoking stuff right? So how does this help shed light on what creatives need to understand?

### Reasons
First, I admit that I make assumptions in order to reach a conclusion. I have to both assume the purpose behind Comedian and the reason that anyone on earth would purchase it. However, I feel that the assumptions can be transposed and the same conclusion can be reached. If all else fails, this is at least one way for creatives to understand what happened with Comedian.

Let's start by assuming Cattelan created Comedian to showcase our propensity to over value the transient. As I stated before, the price tag was always a key point of the piece.

Next we have to assume the reasons anyone would purchase it. Actually, we know the reasons why collectors in Miami purchased it because they told us in an interview; they recognized it's signifcance in the art world and want to preserve and share it. These people are preserving an important moment in art history. Maybe that's how they see themselves, or maybe, that's how they want to be seen. Either way, they've told us their story.

If we continue to consider, we can think of more reasons why someone might buy Comedian. Perhaps a particularly wealthy individual loves popular trends and so acquires the piece. Perhaps another sees the point of Comedian and wishes to purchase it as an ironic reminder of transient value.

I think these examples suffice to explain what creatives need to understand. As a creative, you are helping someone else tell their story. You are helping someone else fulfill their desire and paint the picture of who they are in the world. From helping an art conservationist, to reminding someone what is really valuable to just helping someone validate themselves through what they own, your job is and always has been to help tell those stories.

Comedian is an example of extremes. Extreme absurdity and extreme pricing, but it was always someone's story. Interestingly, Cattelan told his story and has quite successfully made it a part of all our stories.
