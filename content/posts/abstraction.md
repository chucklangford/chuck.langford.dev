---
title: "Abstraction"
date: 2019-11-02T18:18:12-04:00
draft: false
---
I've been told more than once that I "ask good questions", but I've never really understood why. Until now, it's been a happy accident.

I recently finished watching a documentary series on Netflix called Abstract. It was one of the last episodes I watched that helped me clarify where good questions come from. The episode centered on photographer Platon and he said something that resonated with me, "Great design simplifies a very complicated world." That got me thinking.

When it comes to abstraction I have always prefered trying to understand everything in detail. Especially in software. You've got to understand the details! Except, abstraction has it's place in the world, even in engineering (spoiler alert, you can't understand all the details for all technology). As it turns out, abstraction is also closely related to good questions.

### Benefits of Abstraction
Put simply, abstraction helps us understand the world. It acts as a model of the thing we're currently working with. Think of a map. The map shows you the roads, the rivers, lakes, and other major geographical information and it does this without showing you where every tree is located in a region. There's a quote from scientist and philosopher Alfred Korzybski that sums it up nicely, "the map is not the territory."

So why do we need abstraction? Because as we learn we begin to solidify the neural pathways in our brain. Once we have established those pathways, we are then able to make a "map" of the subject. This allows us to simplify the subject enough that we can hold that single, abstract idea in our minds rather than all the details. When an idea is correctly abstracted, we are free to combine it with other ideas making creativity possible.

Abstraction benefits our communication as well. A team, working with the same abstract concept is able to communicate and adapt more easily. They can enact greater creativity because they have a larger pool of ideas and experiences working on the same, abstract concept.

There's a problem though: just like a map, abstractions can be wrong.

### What I Realized
Questions have a tendency to help reveal the weaknesses of an abstraction. Typically, when we're asking questions, we're asking because we simply don't understand. The "map" isn't complete in our mind's eye. Sometimes, questions help us learn what we need to learn and move on. Sometimes, questions shed light on a part of the map no one ever bothered to look at and we all adjust the map together. Sometimes, questions show everyone we've got the map upside down.

The takeaway? Abstractions are good, useful tools but they're not always correct. If we truly don't understand what another person is telling us then we do ourselves a favor by asking questions until we do. We have to put away our fear of looking foolish with questions because it's very likely that others either have the same questions or have accepted an incorrect abstraction. Once we put away the fear, we can all ask good questions.
