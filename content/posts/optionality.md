---
title: "Optionality"
date: 2020-05-26T20:09:00-04:00
draft: false
---
## Introduction
Another idea I'm learning about from the book "Antifragile" is the notion of options. When we think about something that is fragile we can think about it as: something that has no choice in the matter. The glass plate that crashes into the floor has no choice but to break. However, the antifragile has choices. Not only does it have choices, but it potentially has the *option* to improve itself because of the event. Taking from the financial world, an option grants you the "right, but not the obligation."

There are potentially a lot of facets to options that can be explored but, for now, I want to think about just two:

* If we invest in something (time, money, energy), it should provide options with small risk and huge payoff.
* We should really only invest in options that we actually intend to exercise or that we know will likely be exercised.

## Investing
Financially speaking, you invest your assets in the hopes of getting a reward, or a payoff. Of course there is risk involved, but typically (hopefully), you've done the research and feel good about your venture. You attempt to mitigate the risk while optimizing the reward. The same ideas apply when we are creating options for ourselves.

How we use our assets (time, money, energy) informs the options we have available to us in life, work, play, etc. As such, we do ourselves a favor if we invest only in things that have small risk but huge payoff. This automatically means we should avoid huge risks with potentially small payoff. Further, we should only consider risks that don't have the potential to ruin us.

What this looks like outside of finances can be tricky and involves some forward thinking. Here are some examples of potentially bad investments as it might relate to software development:

* Learning an obscure technology that you may never use. As always, learning is never wrong and maybe you'll have a paradigm shift because of your studies but notice the risk involved in unfiltered study. Giving your time and energy to studies that may never be used is a risky investment with an unknown amount of reward.
* Working on a problem for too long. We've all done it. We see a problem that we feel we can solve in an elegant manner and we put our heads down and go to work. Nevermind that something a little more "ugly" would solve the problem; we're engineers! We spend our time and energy on a solve that may not amount to a huge payoff.
* Meetings. This point has been made countless times before but meetings offer an easy way to invest your time and energy and very little payoff.

## Exercising
The other side of this option idea is that we invest in things we intend to actually use, exercise, or benefit from. Investing in options is great, especially if you can get them for cheap or even free. However, options are such that if you never exercise them then there was really no point in making the investment in the first place. The technology industry has no shortage of investment opportunities that may never payoff:

* Learning a technology to mastery when you really only need a specific aspect of it.
* Purchasing books that you may intend to read but there is no clear need for.
* Studying algorithms that potentially will never be needed.
* Designing architecture that derives no benefit.

All of these examples boil down to one thing: time spent. The question to ask yourself is: what am I spending my time on and am I actually using it or will I actually use it? If you're not sure of that, you may have made an investment in an option that you will never exercise.

Now, let's think about a scenario where taking the risk and making the investment is potentially the right thing to do. Consider work that you're doing for your client. Your coding is going well, the problem is well defined, and everything is moving along at a good pace. While coding, you discover an option that neither you or the client knew about. The option isn't necessarily needed but it's easy to imagine being needed and you could easily give the client the option in very little time. This is a good option. The time and effort investment is small. The potential payoff is huge.

## Conclusion
Reading back over this I realize it's easy to think of the ways my examples are wrong, or don't apply, or miss an edge case. The idea isn't to discourage investments, learning, working, effort, or energy; the idea is to point out that sometimes we allow ourselves to invest in things that, if we're being honest, we know won't pay. We have to be more intentional with how we spend our assets, and we need to do this with more than just our careers.
