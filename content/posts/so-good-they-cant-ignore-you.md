---
title: "So Good They Can't Ignore You"
date: 2020-03-28T19:28:12-04:00
draft: false
---

In a recent career related conversation, I received a recommendation to read a book by Cal Newport titled So Good They Can't Ignore You. The title is a quote from Steve Martin and the content of the book revolves around the idea: how to build your career from a place of excellence, rather than passion.

The book is a great read and I would easily recommend it to anyone looking for a mental framework of how to build a career. Although, as I read the book, I found myself wanting to clarify a couple of Newport's ideas.

### The Big Ideas
Without writing what I'm sure would be another synopsis for this book, here are the primary ideas that Newport puts forth:

* "Follow your passion" is bad advice for career building
* Actively work to acquire rare, and valuable skills
* Leverage your skills to acquire autonomy
* Work in the outer limits of your skill and identify a mission. Within those limits, take calculated risks on projects that push you forward. Craft your work to be remarkable.

### My Thoughts
The first clarification I thought this book needed was to define the difference between passion and interest. Let's use the following definitions:

* Passion - Strong or powerful emotion
* Interest - A state of curiosity or concern about or attention to something

I agree that pursuing passion can lead to a less than fulfilling career. However, I don't think this means we can reasonably conclude that unconsidered acquistion of skills is our next best step. After the reader is convinced that passion is not the answer, they are immediately instructed to master skills with no note of interest. To be fair, I'm certain it's assumed that no one will blindly train for something they have no interest in, however, the reader has just finished being convinced that passion is not the answer and the word "interest" is no where to be seen.

My ammendment: Yes, build your career on skills, but build skills that you find interesting and are valuable.

At nearly the same point in the book, Newport introduced some exceptions to the idea that skills are the only thing that matters. These are those moments when skills are trumped by circumstances and we should consider finding a new job:

* The job doesn't allow you to distinguish yourself or develop skills
* The job focuses on something that doesn't inspire you
* The job forces you to work with people you don't like.

All great reasons for moving on to something else. I might add the following to the list:

* For a majority of your time, the job forces you to use tools and processes that don't make you happy.

Forced use of subpar tools and processes can take the joy out of any job, no matter how skilled.

Finally, Newport calls for readers to decide if they need to focus on one, primary skill, or multiple skills. Some careers tend to require a singular focus while others benefit from many skillsets. Excellent advice to be sure. However, I can't help but think it misses an opportunity to talk about the benefits of self improvement and supplementary skills. As a programmer, you can reasonably say that I should always be focusing on improving my software engineering skills, but what about my communication skills, or my social skills, or my leadership skills? These are all part of a typical job; I think Newport's focus may have been too narrow.

Overall, Newport presents a compelling framework for thinking about and working on your career. While this framework is typically more suited for people just starting their careers, there is still plenty of thought provoking value for those of us in the middle of our careers.
